"use strict"

import React, { Component } from 'react' 
import { View, Text, TouchableHighlight, Alert, StyleSheet } from 'react-native'

class loginView extends Component{
    render(){
        return(
            <View>
                <TouchableHighlight onPress={(this.onLogin.bind(this))} style={styles.boton} >
                    <Text style={styles.textoBoton}>Loginn</Text>
                </TouchableHighlight>
            </View>
        )
    }


    onLogin(){
        //console.log("HAs pulsado el boton");
        Alert.alert(
            "Acceso",
            "Te has logueado en el sistema",
            [{
                    text: "Cancelar",
                    onPress: (this.cancelar.bind(this))
                },
                {
                text: "Aceptar",
                onPress: (this.aceptar.bind(this))
            }]
        )
    }

    aceptar(){
        console.log("Aceptar");
    }

    cancelar(){
        console.log("cancelar");
    }
}

const styles = StyleSheet.create({
    boton:{
        width:300,
        height:40,
        backgroundColor:"red",
        alignItems:"center",
        marginTop:10,
        marginBottom:10,
        borderRadius:8,
        borderWidth:1,
        justifyContent: 'center'
    },
    textoBoton:{
        color:"white"    }
});


module.exports = loginView