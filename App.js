import React, { Component } from 'react';
import { Alert, AppRegistry, Button, StyleSheet, View,Text } from 'react-native';

const Login = require ("./src/components/loginView");
const Dash = require ("./src/components/dashboardView");

export default class ButtonBasics extends Component {

  render() {
    return (
      <View style={{alignItems:"center",marginTop:15}}>
        <Text  style={{fontSize:20}}>Welcome to React</Text>
        <Login/>
        <Dash/>
      </View>
    );
  }
}
// skip this line if using Create React Native App
AppRegistry.registerComponent('AwesomeProject', () => ButtonBasics);
